#!/bin/bash
######################################################################
# spring boot application start script
######################################################################

# spring boot 启动参数环境设置
SPRING_BOOT_START_PROFILE="--spring.profiles.active=dev"

# 没有参数传入，直接退出
if [[ $# -le 0 ]]; then
    echo "Usage: $0 [dev|test|pro]  @_@！"
    exit 0
fi

# 判断启动环境
if [[ $1 == "dev" || $1 == "test" || $1 == "probj" || $1 == "progz" ]]; then
    SPRING_BOOT_START_PROFILE="--spring.profiles.active=$1"
else
    echo "Usage: $0 [dev|test|probj|progz]  @_@！"
    exit 0
fi

# 外部指定启动端口号
if [[ $# -ge 2 ]]; then
    SPRING_BOOT_START_PROFILE=${SPRING_BOOT_START_PROFILE}" --server.port=$2"
fi

# 获取脚本所在目录
SHELL_SCRIPT_DIR=$(cd "$(dirname "$0")";pwd)
# log file directory
LOG_DIR="${SHELL_SCRIPT_DIR}/logs"
# console log info
LOG_FILE="${LOG_DIR}/console.out"
# java command
JAVA_CMD="java"
# start param
JAVA_OPS="-server -Xmx1024m -Xms1024m -Djava.security.auth.login.config=${SHELL_SCRIPT_DIR}/config/consumer_jaas.conf -Dcom.sun.management.jmxremote=false -Dcom.sun.management.jmxremote.port=$RANDOM -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false"
# start jar that contains main class
MAIN_JAR="dependency.jar"
# start main class
MAIN_CLASS="Main"
# classpath
CLASSPATH="-cp ${SHELL_SCRIPT_DIR}/lib/*:${SHELL_SCRIPT_DIR}/${MAIN_JAR}:${SHELL_SCRIPT_DIR}/config/"

# 日志文件夹不存在，那么新建
if [[ ! -d "${LOG_DIR}" ]]; then
    mkdir "${LOG_DIR}"
fi

# 启动
nohup ${JAVA_CMD} ${JAVA_OPS} ${CLASSPATH} ${MAIN_CLASS} ${SPRING_BOOT_START_PROFILE} > ${LOG_FILE} 2>&1 &
echo "start successfully! ^_^"
