#!/bin/bash

# 参数不够，直接退出
if [[ $# -le 0 ]]; then
    echo "please input two params which id and id！"
    echo "example: testproject"
    exit 0
fi

# 获取当前目录
CURRENT_DIR=`pwd`
PROJECT_DIR=${CURRENT_DIR}/$2
# 创建工程
gradle init --type java-application
RESOURCES_DIR=${PROJECT_DIR}/src/main/resources
# 创建resources目录
mkdir -p ${RESOURCES_DIR}
# 创建docker构建文件
touch ${CURRENT_DIR}/$2/Dockerfile
echo "mkdir resources dir and Dockerfile successful！"
# 切换到创建的resources目录
cd ${RESOURCES_DIR}
touch bootstrap.yml application.yml application-dev.yml application-test.yml application-pro.yml
# java根目录
PACKAGE_NAME=$1
JAVA_SRC_DIR=../java/${PACKAGE_NAME//./\/}
# 切换到java文件根目录下
cd ${JAVA_SRC_DIR}
# 创建通用包名
mkdir aspect annotation configuration common constant controller entity service dao util interceptor filter cache exception

echo "create project successful！"
