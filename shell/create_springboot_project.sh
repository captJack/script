#!/bin/bash

# 参数不够，直接退出
if [ $# -le 1 ]; then
    echo "please input two params which id and id！"
    echo "example: com.captjack.test testproject"
    exit 0
fi

# 获取当前目录
CURRENT_DIR=`pwd`
RESOURCES_DIR=${CURRENT_DIR}/$2/src/main/resources
# 执行创建
mvn archetype:generate -DgroupId=$1 -DartifactId=$2 -DarchetypeArtifactId=maven-archetype-quickstart
# 创建resources目录
mkdir -p ${RESOURCES_DIR}
echo "mkdir resources successful！"
# 切换到创建的resources目录
cd ${RESOURCES_DIR}
touch bootstrap.yml application.yml application-dev.yml application-test.yml application-pro.yml
# java跟目录
PACKAGE_NAME=$1
JAVA_SRC_DIR=../java/${PACKAGE_NAME//./\/}
# 切换到java文件根目录下
cd ${JAVA_SRC_DIR}
mkdir aspect annotation configuration constant controller entity service dao util

echo "create project successful！"
